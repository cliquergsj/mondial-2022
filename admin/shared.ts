import {
  credential,
  initializeApp,
} from 'firebase-admin'

export function init() {
  console.log('Initializing admin sdk')
  return initializeApp({
    credential: credential.applicationDefault(),
    databaseURL: "https://clique-rgsj-default-rtdb.europe-west1.firebasedatabase.app"
  });
}
