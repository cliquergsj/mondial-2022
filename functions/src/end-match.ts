/* eslint-disable require-jsdoc */
import {
  getFirestore,
  CollectionReference,
  Timestamp,
  QueryDocumentSnapshot,
  DocumentReference,
} from "firebase-admin/firestore";
import {GroupMember, GROUPS_COLLECTION} from "./models/group";
import {
  MATCH_LENGTH,
  MATCHES_COLLECTION,
  Match,
  WinnerType,
  TIE_DIFFERENCE,
} from "./models/match";
import {Morceau, MORCEAUX_COLLECTION} from "./models/morceau";
import {Vote, VOTES_COLLECTION} from "./models/vote";

async function getFinishedMatches() {
  const f = getFirestore();

  const endOfMatch = new Date(new Date().valueOf() - MATCH_LENGTH);

  const matchCol = f.collection(MATCHES_COLLECTION) as
    CollectionReference<Match>;

  const pastMatches = await matchCol
      .where("date", "<", Timestamp.fromDate(endOfMatch))
      .get();

  return pastMatches.docs.filter((d) => d.data().winner == null);
}

async function getChildMatches(parent: QueryDocumentSnapshot<Match>) {
  const f = getFirestore();

  const matchCol = f.collection(MATCHES_COLLECTION) as
    CollectionReference<Match>;

  const leftChildren = await matchCol
      .where("leftParent", "==", parent.ref)
      .get();
  const rightChildren = await matchCol
      .where("rightParent", "==", parent.ref)
      .get();

  return [leftChildren.docs, rightChildren.docs];
}

function getMember(memberPath: string | undefined) {
  const f = getFirestore();

  let memberRef: DocumentReference<GroupMember>;

  if (memberPath) {
    memberRef = f.doc(memberPath) as
      DocumentReference<GroupMember>;
  } else {
    memberRef = f.collection(GROUPS_COLLECTION).doc() as
      DocumentReference<GroupMember>;
  }

  return memberRef.get();
}

function getMorceau(morceauPath: string | undefined) {
  const f = getFirestore();

  let memberRef: DocumentReference<Morceau>;

  if (morceauPath) {
    memberRef = f.doc(morceauPath) as
      DocumentReference<Morceau>;
  } else {
    memberRef = f.collection(MORCEAUX_COLLECTION).doc() as
      DocumentReference<Morceau>;
  }

  return memberRef.get();
}

async function getMorceauFromMember(memberPath: string | undefined) {
  const memberDoc = await getMember(memberPath);
  const memberData = memberDoc.data();

  return getMorceau(memberData?.morceau?.path);
}

async function countVotes(match: QueryDocumentSnapshot<Match>):
    Promise<[left: number, right: number]> {
  const voteCol = match.ref.collection(VOTES_COLLECTION) as
    CollectionReference<Vote>;

  const votes = await voteCol.get();

  const [left, right] = [match.data().left, match.data().right];
  const [leftRef, rightRef] = [
    await getMorceauFromMember(left?.path),
    await getMorceauFromMember(right?.path)];

  let [leftPoints, rightPoints] = [
    leftRef.data()?.handicap || 0,
    rightRef.data()?.handicap || 0];

  votes.forEach((v) => {
    const vote = v.data();
    if (vote.votedFor.path === left?.path) {
      leftPoints++;
    } else if (vote.votedFor.path === right?.path) {
      rightPoints++;
    }
  });

  return [leftPoints, rightPoints];
}

async function setWinner(
    match: QueryDocumentSnapshot<Match>,
    [leftPoints, rightPoints]: [left: number, right: number],
    tieDiff: number,
): Promise<WinnerType> {
  let winner: WinnerType = "tie";
  if (Math.abs(leftPoints - rightPoints) <= tieDiff) {
    winner = "tie";
  } else if (leftPoints > rightPoints) {
    winner = "left";
  } else if (rightPoints > leftPoints) {
    winner = "right";
  }

  await match.ref.update({
    leftPoints, rightPoints, winner,
  });

  return winner;
}

async function updateGroup(
    memberPath: string | undefined,
    side: WinnerType,
    [leftPoints, rightPoints]: [left: number, right: number],
    winner: WinnerType,
) {
  const memberDoc = await getMember(memberPath);
  const memberData = memberDoc.data();
  if (!memberData) {
    return;
  }

  let {
    played,
    won,
    drawn,
    lose,
    goalsFor,
    goalsAgainst,
    goalsDifference,
    points,
  } = memberData;

  played++;
  goalsFor += side === "left" ? leftPoints : rightPoints;
  goalsAgainst += side === "left" ? rightPoints : leftPoints;
  goalsDifference = goalsFor - goalsAgainst;

  switch (winner) {
    case side:
      won++;
      points += 3;
      break;
    case "tie":
      drawn++;
      points += 1;
      break;
    default:
      lose++;
      break;
  }

  await memberDoc.ref.update({
    played,
    won,
    drawn,
    lose,
    goalsFor,
    goalsAgainst,
    goalsDifference,
    points,
  });
}

async function updateKnockout(
    m: QueryDocumentSnapshot<Match>,
    winner: WinnerType
) {
  const f = getFirestore();

  let winnerGroup: DocumentReference<GroupMember> | undefined;
  let loserGroup: DocumentReference<GroupMember> | undefined;

  const [leftPath, rightPath] = [m.data().left?.path, m.data().right?.path];
  if (!leftPath || !rightPath) {
    return;
  }
  const [leftMember, rightMember] = [
    f.doc(leftPath) as DocumentReference<GroupMember>,
    f.doc(rightPath) as DocumentReference<GroupMember>,
  ];

  switch (winner) {
    case "left":
      winnerGroup = leftMember;
      loserGroup = rightMember;
      break;
    case "right":
      winnerGroup = rightMember;
      loserGroup = leftMember;
      break;
  }
  if (!winnerGroup || !loserGroup) {
    console.log("Unable to update knockout. Do it yourself");
    return;
  }

  const [lefts, rights] = await getChildMatches(m);

  if (lefts[0]) {
    await lefts[0].ref.update({left: winnerGroup});
  }
  if (lefts[1]) {
    await lefts[1].ref.update({left: loserGroup});
  }

  if (rights[0]) {
    await rights[0].ref.update({right: winnerGroup});
  }
  if (rights[1]) {
    await rights[1].ref.update({right: loserGroup});
  }
}


export async function endMatches() {
  const finishedMatches = await getFinishedMatches();

  for (const m of finishedMatches) {
    const results = await countVotes(m);

    switch (m.data().type) {
      case "group1":
      case "group2":
      case "group3": {
        const winner = await setWinner(m, results, TIE_DIFFERENCE);

        await updateGroup(m.data().left?.path, "left", results, winner);
        await updateGroup(m.data().right?.path, "right", results, winner);
        break;
      }
      case "eighth":
      case "quarter":
      case "semi": {
        const winner = await setWinner(m, results, 0);

        await updateKnockout(m, winner);
        break;
      }
    }
  }
}
