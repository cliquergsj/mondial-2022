import {DocumentReference, Timestamp} from "firebase/firestore";
import {GroupMember} from "./group";

export const MATCHES_COLLECTION = "matches";

export const MATCH_LENGTH = (45 + 45 + 15) * 1000 * 60;

export const TIE_DIFFERENCE = 6;

export type GroupMatchType = "group1" | "group2" | "group3"
export type KnockoutMatchType = "eighth" | "quarter" | "semi" | "final"
export type MatchType = GroupMatchType | KnockoutMatchType
export type WinnerType = "left" | "right" | "tie"

export interface Match {
  id?: string
  date: Timestamp
  type: MatchType
  left?: DocumentReference<GroupMember>
  leftParent?: DocumentReference<Match>
  leftDesc: string
  right?: DocumentReference<GroupMember>
  rightParent?: DocumentReference<Match>
  rightDesc: string
  leftPoints?: number
  rightPoints?: number
  winner?: WinnerType
}
