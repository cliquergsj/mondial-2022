import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MorceauxRoutingModule } from './morceaux-routing.module';
import { MorceauxComponent } from './morceaux.component';
import { MiniMorceauComponent } from './mini-morceau.component';
import { MatRippleModule } from '@angular/material/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';


@NgModule({
  declarations: [
    MorceauxComponent,
    MiniMorceauComponent
  ],
  imports: [
    CommonModule,
    MorceauxRoutingModule,
    MatRippleModule,
    MatProgressSpinnerModule,
  ]
})
export class MorceauxModule { }
