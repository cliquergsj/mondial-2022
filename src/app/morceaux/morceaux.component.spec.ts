import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MorceauxComponent } from './morceaux.component';

describe('MorceauxComponent', () => {
  let component: MorceauxComponent;
  let fixture: ComponentFixture<MorceauxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MorceauxComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MorceauxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
