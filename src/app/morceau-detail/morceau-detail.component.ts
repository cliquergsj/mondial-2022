import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as md from 'markdown-it'
import { Morceau } from 'functions/src/models/morceau';
import { PlayerService } from '../player/player.service';

@Component({
  selector: 'app-morceau-detail',
  templateUrl: './morceau-detail.component.html',
  styleUrls: ['./morceau-detail.component.scss']
})
export class MorceauDetailComponent implements OnInit {
  morceau!: Morceau;

  private md: md;

  get mdDescription(): string {
    return this.md.render(this.morceau?.description || '')
  }

  get playing(): boolean {
    return this.player.isPlaying(this.morceau)
  }

  constructor(private player: PlayerService,
      activatedRoute: ActivatedRoute) {
    this.md = md()
    activatedRoute.data.subscribe(({ morceau }) => {
      this.morceau = morceau
    })
  }

  ngOnInit(): void {
  }

  togglePlay() {
    if (this.playing) {
      this.player.pause()
    } else {
      this.player.play(this.morceau)
    }
  }

}
