import { TestBed } from '@angular/core/testing';

import { MorceauResolver } from './morceau.resolver';

describe('MorceauResolver', () => {
  let resolver: MorceauResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(MorceauResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
