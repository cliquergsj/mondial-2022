import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MorceauDetailComponent } from './morceau-detail.component';

describe('MorceauDetailComponent', () => {
  let component: MorceauDetailComponent;
  let fixture: ComponentFixture<MorceauDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MorceauDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MorceauDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
