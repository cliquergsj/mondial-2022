import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Morceau } from 'functions/src/models/morceau';
import { Observable } from 'rxjs';
import { MorceauService } from '../morceau/morceau.service';

@Injectable({
  providedIn: 'root'
})
export class MorceauResolver implements Resolve<Morceau|undefined> {
  constructor(private s: MorceauService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Morceau|undefined> {
    const id = route.paramMap.get('id')!
    return this.s.getMorceau$(id)
  }
}
