import { Component, OnInit } from '@angular/core';
import { Match } from 'functions/src/models/match';
import { Observable, of } from 'rxjs';
import { MatchService } from '../match-detail/match.service';

@Component({
  selector: 'app-knockout',
  templateUrl: './knockout.component.html',
  styleUrls: ['./knockout.component.scss']
})
export class KnockoutComponent implements OnInit {

  final$: Observable<Match | undefined> = of(undefined)

  constructor(private service: MatchService) {
  }

  ngOnInit(): void {
    this.final$ = this.service.getMatch$('64')
  }

}
