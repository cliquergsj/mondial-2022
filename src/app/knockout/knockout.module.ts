import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KnockoutRoutingModule } from './knockout-routing.module';
import { KnockoutComponent } from './knockout.component';
import { BracketComponent } from './bracket/bracket.component';
import { KnockoutMatchComponent } from './knockout-match/knockout-match.component';
import { MatRippleModule } from '@angular/material/core';


@NgModule({
  declarations: [
    KnockoutComponent,
    BracketComponent,
    KnockoutMatchComponent,
  ],
  imports: [
    CommonModule,
    KnockoutRoutingModule,
    MatRippleModule,
  ]
})
export class KnockoutModule { }
