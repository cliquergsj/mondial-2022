import { Component, Input, OnChanges } from '@angular/core';
import { Match } from 'functions/src/models/match';
import { Observable, of } from 'rxjs';
import { MatchService } from 'src/app/match-detail/match.service';

@Component({
  selector: 'app-bracket',
  templateUrl: './bracket.component.html',
  styleUrls: ['./bracket.component.scss']
})
export class BracketComponent implements OnChanges {
  @Input() match!: Match
  leftParent$: Observable<Match | undefined> = of(undefined)
  rightParent$: Observable<Match | undefined> = of(undefined)

  constructor(private service: MatchService) { }

  ngOnChanges(): void {
    if (this.match.leftParent) {
      this.leftParent$ = this.service.getMatch$(this.match.leftParent.id)
    } else {
      this.leftParent$ = of(undefined)
    }
    if (this.match.rightParent) {
      this.rightParent$ = this.service.getMatch$(this.match.rightParent.id)
    } else {
      this.rightParent$ = of(undefined)
    }
  }

}
