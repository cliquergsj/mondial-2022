import { Injectable } from '@angular/core';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { NoopScrollStrategy } from '@angular/cdk/overlay'
import { PlayerComponent } from './player.component';
import { Morceau } from 'functions/src/models/morceau';

@Injectable()
export class PlayerService {

  current?: MatBottomSheetRef<PlayerComponent>

  constructor(private bottom: MatBottomSheet) { }

  isPlaying(morceau: Morceau): boolean {
    return !!this.current &&
      this.current.instance?.morceau.mp3 === morceau.mp3 &&
      this.current.instance?.playing
  }

  play(morceau: Morceau) {
    if (!!this.current &&
        this.current.instance?.morceau?.mp3 === morceau.mp3) {
      this.current.instance.play()
      return
    }
    this.current?.dismiss()

    this.current = this.bottom.open(PlayerComponent, {
      hasBackdrop: false,
      closeOnNavigation: false,
      scrollStrategy: new NoopScrollStrategy(),
      data: { morceau },
    })
  }

  pause() {
    this.current?.instance?.pause()
  }
}
