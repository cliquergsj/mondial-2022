import { ChangeDetectorRef, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { Morceau } from 'functions/src/models/morceau';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {
  morceau!: Morceau

  @ViewChild('audio') audio!: ElementRef<HTMLAudioElement>

  get playing(): boolean {
    return this.audio && !this.audio.nativeElement.paused
  }

  constructor(private cdr: ChangeDetectorRef,
    private ref: MatBottomSheetRef<PlayerComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) data: { morceau: Morceau }) {
    this.morceau = data.morceau
  }

  ngOnInit(): void {
    if ('mediaSession' in navigator) {
      navigator.mediaSession.metadata = new MediaMetadata({
        artist: 'Clique de la Royale Garde Saint-Jean de Herve',
        title: this.morceau.name,
      })
    }
  }

  close() {
    this.ref.dismiss()
  }

  play() {
    this.audio.nativeElement.play()
  }
  pause() {
    this.audio.nativeElement.pause()
  }

  refresh() {
    this.cdr.detectChanges()
  }
}
