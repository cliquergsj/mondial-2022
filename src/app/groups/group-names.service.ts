import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { GroupName } from 'functions/src/models/group';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GroupNamesService implements Resolve<string> {

  constructor() { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string | Observable<string> | Promise<string> {
    const group = route.params.group
    if (typeof group === 'string') {
      return this.getLabel(group.toUpperCase() as GroupName)
    }
    return ''
  }

  getLabel(group: GroupName): string {
    const name = {
      'A': 'Tambours',
      'B': 'Adaptations militaires',
      'C': 'Compos oubliées',
      'D': 'Répertoire actuel',
      'E': 'Répertoire actuel',
      'F': 'Compos oubliées',
      'G': 'Adaptations militaires',
      'H': 'Tambours',
    }[group]

    return `Groupe ${group} · ${name}`
  }
}
