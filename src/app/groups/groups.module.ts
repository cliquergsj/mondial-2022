import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupsRoutingModule } from './groups-routing.module';
import { GroupsComponent } from './groups.component';
import { GroupModule } from '../group/group.module';
import { MatTabsModule } from '@angular/material/tabs';


@NgModule({
  declarations: [
    GroupsComponent
  ],
  imports: [
    CommonModule,
    GroupsRoutingModule,
    GroupModule,
    MatTabsModule,
  ]
})
export class GroupsModule { }
