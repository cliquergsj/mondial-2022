import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatchNameService } from '../matches/match-name.service';
import { MatchesComponent } from '../matches/matches.component';
import { MatchesPerTypeComponent } from './matches-per-type.component';

const routes: Routes = [{
  path: '', component: MatchesPerTypeComponent,
  children: [
    {
      path: ':type',
      component: MatchesComponent,
      title: MatchNameService,
    },
    { path: '', redirectTo: 'current', pathMatch: 'full' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatchesPerTypeRoutingModule { }
