import { Inject, Injectable, Optional } from '@angular/core';
import { filter, map, switchMap } from 'rxjs/operators'
import { Auth, user } from '@angular/fire/auth';
import { NEVER, Observable, of } from 'rxjs';
import { DOCUMENT } from '@angular/common';


@Injectable({
  providedIn: 'root',
})
export class UserService {
  readonly isConnected = of(false)
  readonly userID: Observable<string> = NEVER
  readonly displayName$: Observable<string|null> = NEVER
  readonly isAdmin$ = of(false)

  constructor(@Inject(DOCUMENT) private document: Document,
      @Optional() private auth?: Auth) {
    if (auth) {
      this.isConnected = user(auth).pipe(map(u => u !== null))

      this.userID = user(auth).pipe(
          filter(u => u !== null),
          map(u => u!.uid))
      this.displayName$ = user(auth).pipe(
          filter(u => u !== null),
          map(u => u!.displayName))

      this.isAdmin$ = user(auth).pipe(
          switchMap(u => u?.getIdTokenResult() || of(null)),
          map(u => u?.claims?.admin === '2022'),
        )
    }
  }

  async logout() {
    if (this.auth) {
      await this.auth.signOut();

      const domain = 'clique-rgsj.eu.auth0.com'
      const clientID = 'm9Un9IPl63daoO3cPVpkUcgFRQkDZT67'
      const redirectTo = 'https://mondial.cliquergsj.be'

      this.document.location.href = `https://${domain}/v2/logout?client_id=${clientID}&returnTo=${redirectTo}`
    }
  }
}
