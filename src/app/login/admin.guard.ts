import { Injectable } from '@angular/core';
import { CanActivate, CanLoad } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate, CanLoad {
  constructor(private user: UserService) { }

  canActivate(): Observable<boolean> {
    return this.user.isAdmin$
  }
  canLoad(): Observable<boolean> {
    return this.user.isAdmin$
  }
}
