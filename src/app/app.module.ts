import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { connectFirestoreEmulator, enableIndexedDbPersistence, getFirestore, provideFirestore } from '@angular/fire/firestore';
import { connectFunctionsEmulator, getFunctions, provideFunctions } from '@angular/fire/functions';
import { environment } from 'src/environments/environment';
import { MatTabsModule } from '@angular/material/tabs'
import { registerLocaleData } from '@angular/common';
import localeFrBE from '@angular/common/locales/fr-BE';
import localeFrBEExtra from '@angular/common/locales/extra/fr-BE';
import { IndexComponent } from './index/index.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { UserComponent } from './user/user.component';
import { PlayerModule } from './player/player.module';

registerLocaleData(localeFrBE, 'fr-BE', localeFrBEExtra)
@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    UserComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule,
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(() => {
      const firestore = getFirestore()
      if (!environment.production) {
        connectFirestoreEmulator(firestore, 'localhost', 8080)
      }
      enableIndexedDbPersistence(firestore)
      return firestore
    }),
    provideFunctions(() => {
      const functions = getFunctions()
      if (!environment.production) {
        connectFunctionsEmulator(functions, 'localhost', 5001)
      }
      return functions
    }),
    PlayerModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatTabsModule,
    MatToolbarModule,
  ],
  providers: [{
    provide: LOCALE_ID, useValue: 'fr-BE',
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
