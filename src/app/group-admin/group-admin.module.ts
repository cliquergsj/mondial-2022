import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupAdminComponent } from './group-admin.component';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';



@NgModule({
  declarations: [
    GroupAdminComponent
  ],
  exports: [
    GroupAdminComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatSelectModule,
  ]
})
export class GroupAdminModule { }
