import { Component, OnInit } from '@angular/core';
import { GroupMember } from 'functions/src/models/group';
import { Morceau } from 'functions/src/models/morceau';
import { Observable, of } from 'rxjs';
import { GroupService } from '../group-table/group.service';
import { MorceauService } from '../morceau/morceau.service';

@Component({
  selector: 'app-group-admin',
  templateUrl: './group-admin.component.html',
  styleUrls: ['./group-admin.component.scss']
})
export class GroupAdminComponent implements OnInit {

  members$: Observable<GroupMember[]> = of([])

  displayedColumns = [
    'id',
    'name',
    'points',
    'played',
    'won',
    'drawn',
    'lose',
    'goalsFor',
    'goalsAgainst',
    'goalsDifference',
  ]
  morceaux$: Observable<Morceau[]> = of([])

  constructor(private service: GroupService,
    private morceauxService: MorceauService) {}

  ngOnInit(): void {
    this.members$ = this.service.getMembersOfGroup$()
    this.morceaux$ = this.morceauxService.getMorceaux$()
  }

  update(group: GroupMember, morceau: string) {
    this.service.update(group.id!, morceau)
  }

  trackBy(_: number, item: GroupMember): any {
    return item.id
  }

}
