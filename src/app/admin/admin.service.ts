import { Injectable } from '@angular/core';
import { collection, CollectionReference, doc, Firestore, setDoc, Timestamp } from '@angular/fire/firestore';
import { Functions, httpsCallable,  } from '@angular/fire/functions';
import { GroupMember, GroupName, GROUPS_COLLECTION } from 'functions/src/models/group';
import { Match, MATCHES_COLLECTION } from 'functions/src/models/match';
import { defaultMatches } from './matches';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private firestore: Firestore,
      private fun: Functions) { }

  initGroups() {
    const groups: GroupName[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    const col = collection(this.firestore, GROUPS_COLLECTION) as CollectionReference<GroupMember>
    groups.forEach(g => {
      [1, 2, 3, 4].forEach(async i => {
        await setDoc(doc(col, `${g}${i}`), {
          group: g,
          played: 0,
          won: 0,
          drawn: 0,
          lose: 0,
          goalsFor: 0,
          goalsAgainst: 0,
          goalsDifference: 0,
          points: 0,
        })
      })
    })
  }

  initMatches() {
    const matches = collection(this.firestore, MATCHES_COLLECTION) as CollectionReference<Match>
    const groups = collection(this.firestore, GROUPS_COLLECTION) as CollectionReference<GroupMember>
    defaultMatches.forEach(async m => {
      const match: Match = {
        date: Timestamp.fromDate(new Date(m.date)),
        type: m.type,
        leftDesc: m.leftDesc,
        rightDesc: m.rightDesc,
      }
      if (m.left) {
        match.left = doc(groups, m.left)
      }
      if (m.leftParent) {
        match.leftParent = doc(matches, m.leftParent)
      }
      if (m.right) {
        match.right = doc(groups, m.right)
      }
      if (m.rightParent) {
        match.rightParent = doc(matches, m.rightParent)
      }

      await setDoc(doc(matches, m.id), match)
    })
  }

  async endMatches() {
    const endMatches = httpsCallable(this.fun, 'endMatches');
    await endMatches()
  }
}
