import { Component } from '@angular/core';
import { TIE_DIFFERENCE } from 'functions/src/models/match';

@Component({
  selector: 'app-infos',
  templateUrl: './infos.component.html',
  styleUrls: ['./infos.component.scss']
})
export class InfosComponent {
  tieDiff = TIE_DIFFERENCE
}
