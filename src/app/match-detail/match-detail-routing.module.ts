import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatchDetailComponent } from './match-detail.component';
import { MatchResolver } from './match.resolver';

const routes: Routes = [{
  path: ':id',
  component: MatchDetailComponent,
  resolve: {
    match: MatchResolver,
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatchDetailRoutingModule { }
