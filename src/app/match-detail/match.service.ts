import { Injectable } from '@angular/core';
import {
  collection,
  collectionData,
  CollectionReference,
  Firestore,
  orderBy,
  query,
} from '@angular/fire/firestore';
import { Match, MATCHES_COLLECTION, MatchType, MATCH_LENGTH } from 'functions/src/models/match';
import { ConnectableObservable, Observable } from 'rxjs';
import { map, publishReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MatchService {
  col: CollectionReference<Match>

  private matches$: Observable<readonly Match[]>

  private connect() {
    (this.matches$ as ConnectableObservable<any>).connect()
  }

  get endOfMatch(): Date {
    return new Date(new Date().valueOf() - MATCH_LENGTH)
  }

  constructor(firestore: Firestore) {
    this.col = collection(firestore, MATCHES_COLLECTION) as CollectionReference<Match>

    this.matches$ = collectionData(
      query(this.col, orderBy('date', 'asc')),
      { idField: 'id' }).pipe(publishReplay(1))
  }

  getMatch$(matchID: string): Observable<Match | undefined> {
    this.connect()
    return this.matches$.pipe(map(mm => mm.find(m => m.id === matchID)))
  }

  getMatches$(): Observable<readonly Match[]> {
    this.connect()
    return this.matches$
  }

  getCurrent(): MatchType {
    const now = (new Date()).toISOString()
    if (now >= '2022-12-14T22:00:00.000Z') {
      return 'final'
    } else if (now >= '2022-12-10T22:00:00.000Z') {
      return 'semi'
    } else if (now >= '2022-12-06T22:00:00.000Z') {
      return 'quarter'
    } else if (now >= '2022-12-02T22:00:00.000Z') {
      return 'eighth'
    } else if (now >= '2022-11-28T22:00:00.000Z') {
      return 'group3'
    } else if (now >= '2022-11-24T22:00:00.000Z') {
      return 'group2'
    }
    return 'group1'
  }

  getMatchesByType$(type: MatchType): Observable<Match[]> {
    this.connect()
    return this.matches$.pipe(map(mm => mm.filter(m => m.type === type)))
  }

  getMatchesByGroup$(group: string): Observable<Match[]> {
    this.connect()

    const groupTypes: MatchType[] = ['group1', 'group2', 'group3']

    return this.matches$.pipe(map(mm => {
      return mm.filter(m => {
        return groupTypes.includes(m.type) && m.left?.id.startsWith(group.toUpperCase());
      });
    }))
  }

  recentMatches$(limit: number): Observable<Match[]> {
    this.connect()

    return this.matches$.pipe(
      map(mm => {
        let idx = mm.findIndex(({ date }) => date.toDate() >= this.endOfMatch)
        idx -= limit/2
        if (idx < 0) {
          idx = 0
        }

        return mm.slice(idx, idx + limit)
      }),
    )
  }

  nextMatch$(cur: Match): Observable<Match> {
    this.connect()

    return this.matches$.pipe(
      map(mm => {
        const idx = mm.findIndex(m => m.id === cur.id) + 1
        return mm[idx]
      })
    )
  }

  previousMatch$(cur: Match): Observable<Match> {
    this.connect()

    return this.matches$.pipe(
      map(mm => {
        const idx = mm.findIndex(m => m.id === cur.id) - 1
        return mm[idx]
      })
    )
  }

}
