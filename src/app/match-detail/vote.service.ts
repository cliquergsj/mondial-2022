import { Injectable } from '@angular/core';
import {
  collection,
  CollectionReference,
  deleteDoc,
  doc,
  docData,
  DocumentReference,
  setDoc,
} from '@angular/fire/firestore';
import { GroupMember } from 'functions/src/models/group';
import { Vote, VOTES_COLLECTION } from 'functions/src/models/vote';
import { Observable } from 'rxjs';
import { first, map, shareReplay, switchMap } from 'rxjs/operators';
import { UserService } from '../login/user.service';
import { MatchService } from './match.service';

@Injectable({
  providedIn: 'root',
})
export class VoteService {
  private cache = new Map<string, Observable<Vote | undefined>>()

  constructor(private matches: MatchService,
    private user: UserService) {
  }

  private getVoteDoc$(match: string): Observable<DocumentReference<Vote>>   {
    const col = collection(
      doc(this.matches.col, match),
      VOTES_COLLECTION) as CollectionReference<Vote>
    
    return this.user.userID.pipe(
      map(uid => doc(col, uid)))
  }

  private createVote$(match: string): Observable<Vote | undefined> {
    return this.getVoteDoc$(match)
      .pipe(switchMap(m => docData(m)))
  }

  getVote$(match: string): Observable<Vote | undefined> {
    if (this.cache.has(match)) {
      return this.cache.get(match)!
    }

    const vote$ = this.createVote$(match).pipe(shareReplay({
      bufferSize: 1,
      refCount: false,
    }))
    this.cache.set(match, vote$)
    return vote$
  }

  async voteFor(match: string, morceau: DocumentReference<GroupMember> | undefined) {
    const voteDoc = await this.getVoteDoc$(match)
      .pipe(first())
      .toPromise()

    if (morceau === undefined) {
      return deleteDoc(voteDoc)
    }

    return setDoc(voteDoc, { votedFor: morceau })
  }
}
