import { TestBed } from '@angular/core/testing';

import { MatchNameService } from './match-name.service';

describe('MatchNameService', () => {
  let service: MatchNameService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatchNameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
