import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatchesComponent } from './matches.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MiniMatchComponent } from './mini-match.component';
import { RouterModule } from '@angular/router';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    MatchesComponent,
    MiniMatchComponent,
  ],
  exports: [
    MatchesComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    MatIconModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatRippleModule,
  ]
})
export class MatchesModule { }
