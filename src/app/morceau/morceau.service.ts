import { Inject, inject, InjectionToken } from '@angular/core';
import { Injectable } from '@angular/core';
import { addDoc, collection, collectionData, CollectionReference, doc, DocumentReference, Firestore, setDoc } from '@angular/fire/firestore';
import { GroupMember, GROUPS_COLLECTION } from 'functions/src/models/group';
import { Morceau, MORCEAUX_COLLECTION } from 'functions/src/models/morceau';
import { ConnectableObservable, Observable, of } from 'rxjs';
import { map, tap, publishReplay, switchMap } from 'rxjs/operators';
import { GroupService } from '../group-table/group.service';

export const morceauxCollection = new InjectionToken('morceaux', {
  providedIn: 'root',
  factory: () => collection(inject(Firestore), MORCEAUX_COLLECTION) as CollectionReference<Morceau>
})

function isDocRefOfGroupMember(docRef: DocumentReference<any>): docRef is DocumentReference<GroupMember> {
  return docRef.path.startsWith(`${GROUPS_COLLECTION}/`)
}

export type MorceauxPerGroup = [Morceau, Morceau, Morceau, Morceau]

@Injectable({
  providedIn: 'root'
})
export class MorceauService {

  private morceaux$: Observable<Morceau[]>

  private connect() {
    (this.morceaux$ as ConnectableObservable<any>).connect()
  }

  constructor(@Inject(morceauxCollection) private col: CollectionReference<Morceau>,
      private groupService: GroupService) {
    this.morceaux$ = collectionData(col, { idField: 'id' }).pipe(publishReplay(1))
  }
      
  getMorceau$(
    a?: DocumentReference<Morceau> | DocumentReference<GroupMember> | string,
  ): Observable<Morceau | undefined> {
    this.connect()

    if (typeof a === 'string') {
      return this.morceaux$.pipe(map(mm => mm.find(m => m.id === a)))
    }
    if (!a) return of(undefined)
    if (isDocRefOfGroupMember(a)) {
      return this.groupService.getGroupMember$(a)
        .pipe(switchMap(g => {
          if (g && g.morceau) {
            return this.getMorceau$(g.morceau)
          }
          return of(undefined)
        }))
    }
    return this.getMorceau$(a.id)
  }

  getMorceaux$(): Observable<Morceau[]> {
    this.connect()
    return this.morceaux$
      .pipe(tap(() => {})) // workaround https://github.com/angular/components/issues/25483
  }

  async update({id, ...morceau}: Partial<Morceau>) {
    if (id) {
      await setDoc(doc(this.col, id), morceau)
    } else {
      await addDoc(this.col, morceau)
    }
  }
}
