import { AfterViewInit, Component, Injector, ViewChild, ViewContainerRef } from '@angular/core';
import { LIMIT_TOKEN, ListType, LIST_TOKEN } from '../matches/matches.component';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements AfterViewInit {

  @ViewChild('recentMatches', { read: ViewContainerRef })
  recentMatches!: ViewContainerRef

  constructor(private _injector: Injector) {
  }

  ngAfterViewInit() {
    this.displayMatchList(this.recentMatches, 'recent')
  }

  private async displayMatchList(vcr: ViewContainerRef, list: ListType) {
    const { MatchesComponent } = await import('../matches/matches.component')

    const injector = Injector.create({
      providers: [
        { provide: LIST_TOKEN, useValue: list },
        { provide: LIMIT_TOKEN, useValue: 8 },
      ],
      parent: this._injector,
    })

    vcr.createComponent(MatchesComponent, {
      injector,
    })
  }
}
