import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MorceauAdminComponent } from './morceau-admin.component';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';



@NgModule({
  declarations: [
    MorceauAdminComponent
  ],
  exports: [
    MorceauAdminComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
  ]
})
export class MorceauAdminModule { }
