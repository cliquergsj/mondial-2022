import { inject, Inject, Injectable, InjectionToken } from '@angular/core';
import {
  collection,
  collectionData,
  CollectionReference,
  doc,
  DocumentReference,
  Firestore,
  query,
  QueryConstraint,
  updateDoc,
  where,
} from '@angular/fire/firestore';
import { GroupMember, GroupName, GROUPS_COLLECTION } from 'functions/src/models/group';
import { Match } from 'functions/src/models/match';
import { Morceau } from 'functions/src/models/morceau';
import { combineLatest, ConnectableObservable } from 'rxjs';
import { Observable } from 'rxjs';
import { map, publishReplay } from 'rxjs/operators';
import { MatchService } from '../match-detail/match.service';
import { morceauxCollection } from '../morceau/morceau.service';

export const groupCollection = new InjectionToken('groups', {
  providedIn: 'root',
  factory: () => collection(inject(Firestore), GROUPS_COLLECTION) as CollectionReference<GroupMember>
})

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  private groupMembers$: Observable<GroupMember[]>

  private connect() {
    (this.groupMembers$ as ConnectableObservable<any>).connect()
  }

  constructor(@Inject(groupCollection) public col: CollectionReference<GroupMember>,
      private matchService: MatchService,
      @Inject(morceauxCollection) private morceaux: CollectionReference<Morceau>) {
    this.groupMembers$ = collectionData(col, { idField: 'id' }).pipe(publishReplay(1))
  }

  getGroupMember$(
    a: DocumentReference<GroupMember> | string,
  ): Observable<GroupMember | undefined> {
    this.connect()
    let id: string
    if (typeof a === 'string') {
      id = a
    } else {
      id = a.id
    }
    
    return this.groupMembers$.pipe(map(mm => mm.find(m => m.id === id)))
  }

  getMembersOfGroup$(group?: GroupName): Observable<GroupMember[]> {
    const matches$ = this.matchService.getMatches$()

    const constraints: QueryConstraint[] = []
    if (group) {
      constraints.push(where('group', '==', group.toUpperCase()))
    }

    const rawMembers$ = collectionData(
      query(this.col, ...constraints),
      { idField: 'id' })


    return combineLatest([rawMembers$, matches$]).pipe(
      map(([members, matches]) => {
        return members.sort((a, b) => {
          return this.sortGroup(a, b, matches)
        })
      })
    )
  }

  private sortGroup(a: GroupMember, b: GroupMember, matches: readonly Match[]): number {
    let diff = b.points - a.points
    if (diff !== 0) {
      return diff
    }

    const matchesPlayed = matches.filter(m => {
      return (m.left?.id === a.id || m.left?.id === b.id) &&
        (m.right?.id === a.id || m.right?.id === b.id)
    })
    diff = this.higherPoints(matchesPlayed, a, b);
    if (diff !== 0) {
      return diff
    }
    diff = this.higherGoalsDiff(matchesPlayed, a, b);
    if (diff !== 0) {
      return diff
    }
    diff = this.higherGoalsScored(matchesPlayed, a, b);
    if (diff !== 0) {
      return diff
    }

    // 4. Superior goal difference in all group matches
    diff = b.goalsDifference - a.goalsDifference
    if (diff !== 0) {
      return diff
    }
    // 5. Higher number of goals scored in all group matches
    diff = b.goalsFor - a.goalsFor
    if (diff !== 0) {
      return diff
    }
    // 6. Higher number of wins in all group matches
    diff = b.won - a.won
    if (diff !== 0) {
      return diff
    }

    return 0
  }

  // 1. Higher number of points obtained in the matches played between the teams in question
  private higherPoints(matchesPlayed: Match[], a: GroupMember, b: GroupMember) {
    let aPoints = 0;
    let bPoints = 0;
    matchesPlayed.forEach(m => {
      if (m.winner === 'tie') {
        aPoints += 1;
        bPoints += 1;
      } else if ((m.left?.id === a.id && m.winner === 'left') ||
        (m.left?.id === b.id && m.winner === 'right')) {
        aPoints += 3;
      } else if ((m.left?.id === a.id && m.winner === 'right') ||
        (m.left?.id === b.id && m.winner === 'left')) {
        bPoints += 3;
      }
    });
    return bPoints - aPoints
  }

  // 2. Superior goal difference resulting from the matches played between the teams in question
  private higherGoalsDiff(matchesPlayed: Match[], a: GroupMember, b: GroupMember) {
    let aPoints = 0;
    let bPoints = 0;
    matchesPlayed.forEach(m => {
      if (m.leftPoints == null || m.rightPoints == null) {
        return
      }
      if (m.left?.id === a.id) {
        aPoints += m.leftPoints - m.rightPoints
        bPoints += m.rightPoints - m.leftPoints
      } else {
        bPoints += m.leftPoints - m.rightPoints
        aPoints += m.rightPoints - m.leftPoints
      }
    });
    return bPoints - aPoints
  }

  // 3. Higher number of goals scored in the matches played between the teams in question;
  private higherGoalsScored(matchesPlayed: Match[], a: GroupMember, b: GroupMember) {
    let aPoints = 0;
    let bPoints = 0;
    matchesPlayed.forEach(m => {
      if (m.leftPoints == null || m.rightPoints == null) {
        return
      }
      if (m.left?.id === a.id) {
        aPoints += m.leftPoints
        bPoints += m.rightPoints
      } else {
        bPoints += m.leftPoints
        aPoints += m.rightPoints
      }
    });
    return bPoints - aPoints
  }


  update(id: string, morceauId: string) {
    const member = doc(this.col, id)
    const morceau = doc(this.morceaux, morceauId)

    updateDoc(member, { morceau })
  }
}
