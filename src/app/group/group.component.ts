import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GroupName } from 'functions/src/models/group';
import { GroupNamesService } from '../groups/group-names.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent {
  group!: GroupName

  constructor(public nameServ: GroupNamesService,
      route: ActivatedRoute) {
    route.params.subscribe(({ group }) => {
      if (typeof group === 'string') {
        this.group = group.toUpperCase() as GroupName
      }
    })
  }
}
