// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBF-e8bJgeXfGb2Dxw_EM546tHEaJfZAgI",
    authDomain: "clique-rgsj.firebaseapp.com",
    projectId: "clique-rgsj",
    storageBucket: "clique-rgsj.appspot.com",
    messagingSenderId: "822882469840",
    appId: "1:822882469840:web:ae120b51db97201ca6dbd5",
    measurementId: "G-SC9ZZQ7PXK"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
